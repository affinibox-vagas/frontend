## Apresentação

A Affinibox é uma plataforma de gestão de Clubes de Parcerias.
Oferecemos uma solução whitelabel para associações e empresas que queiram disponibilizar suas parcerias a seus colaboradores de forma mensurável e prática. 
Parte do nosso trabalho é prospectar novas parcerias e temos alguns grandes nomes.
A composição da empresa é bastante enxuta e a gestão é bastante horizontalizada, assim, as decisões são bastante rápidas.
Todos no time tem influência em como o produto se desenvolve e como os problemas dos nossos clientes são resolvidos.


## Descrição da vaga

Procuramos um Dev. Frontend Pleno/Sênior com bons conhecimentos em UI/UX.

Hoje nos baseamos (e muito) nos conceitos do Material Design, porém, não vemos problema em adaptá-lo ao nosso cenário.

Você trabalhará junto ao nosso Designer para criar experiências incríveis para os usuários da nossa plataforma! =)

## Local

Escritório, São Paulo - Entre a Oscar Freire e Haddock Lobo, relativamente perto da Paulista, muito próxima a estação Oscar Freire.

## Benefícios

Plano de saúde (Omint)

Vale Refeição (~30/dia)

Vale Alimentação (~450/mês)

Vale Transporte

Horário flexível

## Requisitos

* **Obrigatórios:**
    * React, todo seu ecossistema, patterns e novidades (redux, thunks, router, etc..)
    * Organização e documentação de código
    * Criticidade ao escolher libs/frameworks
    * Testes E-E (gostamos do nightwatch.js)
    * Boa vontade em ensinar e aprender com as outras pessoas do time

* **Desejáveis:**
    * React Native (Expo)
    * Google Analytics
    * Ensino Superior Completo
    * Conhecimentos em CI/CD

## Contratação

CLT (40h), salário a combinar.


## Como se candidatar

Envie um e-mail para `emanuel.meli@affinibox.com.br` com o assunto [Vaga Frontend] {SEU_NOME}

O e-mail deve conter:
* Breve apresentação
* CV ou Linkedin (preferimos Linkedin!)
* GitHub